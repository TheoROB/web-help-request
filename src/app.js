const url_user ='https://web-help-request-api.herokuapp.com/users';
const url_ticket ='https://web-help-request-api.herokuapp.com/tickets';
const userSelect = document.getElementById("userSelect");
const addPost = document.getElementById("add-post-form");
const newDes = document.getElementById("newDes");
const table = document.getElementById("table");
let tableau_user = [];

// Get Récuperer users API
// methode GET 

function afficherUser(){
    fetch(url_user)
    .then((resp) => resp.json())
    .then(function(users) {
      let user = users.data;
      user.forEach( users => {
        // console.log(users)
        tableau_user[users.id] = users.username
        userSelect.innerHTML += `
            <option value="${users.id}">${tableau_user[users.id]}</option>`
    })
    afficherInfos()
    
  })
}

// Get Récuperer tickets API

function afficherInfos() {
  fetch(url_ticket)
  .then((resp) => resp.json())
  .then(function (infos) {
    let info = infos.data;
    info.forEach( infos => {
      console.log(info);
      table.innerHTML += `
      <tr>
          <th scope="row">${infos.id}</th>
          <td>${tableau_user[infos.users_id]}</td>
          <td>${infos.subject}</td>
          <td>${infos.date}</td>
          <td><button id="delBtn" class="rounded border">Passer</button></td>
      </tr>`
    })
  })
}

addPost.addEventListener('submit', (e) => {
  e.preventDefault();
  console.log(newDes.value)
  let delBtn = e.target.id =='delBtn'
  let id = e.target.parentElement.dataset.id;

  if(delBtn){
    fetch(`${url_ticket}/${id}`, {method: 'DELETE',})
    .then(res => res.json())
    .then(() => location.reload)
  }

  fetch(url_ticket,{
    method: 'POST',
    body: new URLSearchParams({
      subject: newDes.value,
      userId: userSelect.value
    })
  })
  .then(res => res.json())
  .then(data => {
    const dataArr = [];
    dataArr.push(data);
  })
})

afficherUser()
